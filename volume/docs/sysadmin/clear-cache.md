*Clear cache to free up memory*

Writing to this will cause the kernel to drop clean caches, as well as reclaimable slab objects like dentries and inodes. Once dropped, their memory becomes free.

To free pagecache:
```bash
sync;echo 1 > /proc/sys/vm/drop_caches
```
To free reclaimable slab objects (includes dentries and inodes):
```bash
sync;echo 2 > /proc/sys/vm/drop_caches
```
To free slab objects and pagecache:
```bash
sync;echo 3 > /proc/sys/vm/drop_caches
```
This is a non-destructive operation and will not free any dirty objects. Use of this file can cause performance problems. Since it discards cached objects, it may cost a significant amount of I/O and CPU to recreate the dropped objects, especially if they were under heavy use.

“sync” only makes dirty cache to clean cache. cache is still preserved. drop_caches doesn’t touch dirty caches and only drops clean caches. So to make all memory free, it is necessary to do sync first before drop_caches in case flushing daemons hasn’t written the changes to disk.
