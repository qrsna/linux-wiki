# Remove sudo password prompt

**Open sudoers config file**
```bash
sudo visudo 
```
**Edit or Add the group the user is apart of. All users in this group passwords will be removed**
```bash
%sudo ALL=(ALL) NOPASSWD: ALL 
```
