## Create SSH key

**Generate ssh key**  
<s>`ssh-keygen -C "your_email@example.com"`</s>

**Generate secure ssh key**
```bash 
ssh-keygen -o -a 100 -t ed25519 -C "your_email@example.com"
```
`-C` Comment - Use any identifier: name/username/email/etc


**Copy SSH key from pc to server**
```bash
ssh-copy-id username@remote_host 
```

## SSH config
**Create config for a quick way to connect via ssh**
```bash
vi /home/john/.ssh/config 

Host iandi
HostName iandi.co.za
User john
IdentityFile /home/john/.ssh/iandi 
```

**Your config file you just created will allow you to connect via SSH much faster**
```bash
ssh iandi 
```


