**Find and replace using REGEX**    
String to replace `surf_prime_time_r3vamp /`
```bash
sed -i -E "s,surf_[A-Za-z0-9]+_?-?[A-Za-z0-9]+_?[A-Za-z0-9]+\s/, 123," workmaps.txt 
```
`-i` input file   
`-E` Regex

**Remove empty lines**
```bash
sed -i -E "s,^\s*$,," workmaps.txt 
```

**Remove the first character sed**
```bash
cat input_file | sed 's/^..//' > output_file
```


**Search and replace text in multiple files and escape special characters**
```bash
sed -i 's/test.data\*\.keyboard/test\.\*\.keyboard/g' *
```
Before: `test.data*.keyboard`
After: `test.*.keyboard`

**Escape Characters**
```bash
sed -i 's/\(foo\)/bin/g' file.json
```

**Replace string but keep indention or spaces**
```bash
sed -i  '/"interval":/i \ \ \ \ \ \ "occurrences": 2,' file.json
```

**Append line after match**
```bash
sed  '/\[option\]/a Hello World' input
```

**Insert line before match**
```bash
sed  '/\[option\]/i Hello World' input
```

**Search and replace string**
```bash
find . -type f -name "gamemode_comp*" -exec sed -i 's/YB53XUH3/"J123f34J123454!^dfg"/g' {} + 
```
Before: `[ ] surf_zion.nav.bz2 2015-06-21 18:27 132`
After: `surf_whoknows.nav.bz2`
