**Find and replace string**
```
grep -rl "gluster/glusterfs-3.7" . | xargs sed -i 's/glusterfs-3.7/glusterfs-3.13/g' 
```
Or
```
rg keyword --files-with-matches | xargs sed -i 's/keyword/repacekeyword/g'
```
*`rg` is a `grep` replacement*


**Find all matching files and replace with new file**
```bash
find ~/ -type f -iname "gameme.smx" | while read line
do
  cp -v gameme.smx $line
done
```

**Find and remove permissions**
```bash
find . -type f -iname "*.ini" -exec chmod -x {} \;
```

**Find files and grep**
```bash
find ~/ -type f -name "autoexec.cfg" -exec cat {} | grep 'log_address' \;
```

**Find and replace text in multiple files**
```bash
sed -i 's/"findtext"/"replacetext"/g' *
```

**Find text and replace the entire line**
```bash
sed -i 's/"^findtext.*"/"replacetext"/g' *
```

**Delete a specific subfolder within all directories**
```bash
echo ~/john/*/csgo/csgo/ | xargs -n 1 rm -r 
```

**Copy a specific sub directory from all directories.**
```bash
echo ~/john/*/ | xargs -n 1 cp -R csgo/
```
The -i option of cp command means that you will be asked whether to overwrite a file


**Find empty files and delete**
```
find /tmp -type f -empty -delete
```

**Find and replace string**
```bash
find . -type f -name "gamemode_comp*" -exec sed -i 's/123/dfg/g' {} + 
```
