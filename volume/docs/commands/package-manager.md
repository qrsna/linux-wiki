## Debian / Ubuntu
**list repos**
```
sudo apt-cache policy
```

**add repo**
```
sudo add-apt-repository "deb http://us.archive.ubuntu.com/ubuntu/ saucy universe multiverse"
```

**remove repo**
```
sudo add-apt-repository --remove <ppa:whatever/ppa>
```

### apt

**Update the APT package index**
```
sudo apt update
```

**List all packages (Installed or not) containing the word “cow” followed by anything**
```
apt list cow*
```

**List installed packages**
```
apt list --installed
```

**Searches for “cow” in both package names and descriptions **
```
apt search cow
```

**Shows detailed info about package**
```
apt show cowsay
```

**Install Package**
```
sudo apt install cowsay
```

**Remove package**
```
sudo apt remove cowsay
```

**Remove package and config**
```
sudo apt purge cowsay
```

**List upgradable packages**
```
apt list --upgradeable
```

**Upgrade single package**
```
sudo apt install <package_name> --only-upgrade
```

**Upgrade packges**
```
sudo apt upgrade
```

**Upgrade distro**
```
sudo apt full-upgrade
```


**Remove stale packages**
```
sudo apt autoremove
```

### apt-get
**Find packages**
```bash
apt-cache search 
```

**Find installed packages(Ubuntu)**
```bash
dpkg -l | grep php 
```

**Update a single package using the CLI (Ubuntu)**
```bash
sudo apt-get --only-upgrade install <package_name> 
```    

**List packages**
```
dpkg --get-selections | grep -v deinstall
```

**List packages versions**
```
sudo apt-cache policy <package_name>
```

**List packages, with updates**
```
sudo apt-get -u upgrade
```

**Upgrade system**
```
sudo apt-get dist-upgrade
```

**Remove stale packages**
```
sudo apt-get autoremove
```

## Centos/Redhat

**Install Package**
```
sudo yum install <package_name>
```

**Uninstall Package**
```
sudo yum remove <package_name>
```

**Update package**
```
sudo yum update <package_name>
```

**List available packages**
```
sudo yum list
```

**List installed packages**
```
sudo yum list installed
```

**Clear cache**
```
sudo yum clean all
```

## Arch

**Update package list**
```
sudo pacman -Syy
```

**Update and upgrade all**
```
sudo pacman -Syu
```

**Install specific package**
```
sudo pacman -S <package_name> 
```

**Find available packages**
```
sudo pacman -Ss <keyword>
```

**Find available local packages**
```
sudo pacman -Qs <keyword>
```

**List all files from package**
```
pacman -Ql <package_name>
```

**View user install packages**
```
comm -23 <(pacman -Qqett) <(pacman -Qqg base -g base-devel | sort | uniq)
```
