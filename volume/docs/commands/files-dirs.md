**Copy files locally faster**
```
rsync -avhW --no-compress --progress /src/ /dst/
```

```
-a is for archive, which preserves ownership, permissions etc.
-v is for verbose, so I can see what's happening (optional)
-h is for human-readable, so the transfer rate and file sizes are easier to read (optional)
-W is for copying whole files only, without delta-xfer algorithm which should reduce CPU load
--no-compress as there's no lack of bandwidth between local devices
--progress so I can see the progress of large files (optional)
```

**Backup all changed config files**

```bash
debsums -ce | tar --files-from=- -cf configs.tar 
```

**Directory size disincluding sub directory info**
```bash
du -hs /path/to/directory 
```

**Number of files in dir**
```bash
ls | wc -l
```

**Redirecting the standard error (stderr) and stdout to file**
```bash
command > file 2>&1  
```

**Compare files and directories content**
```bash
diff file1 file2
diff /root/dir1/ /root/dir2/
```

**Compare file content with visual output</code>**
```bash
sdiff file1 file2
```
