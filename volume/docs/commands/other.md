**Certbot - generate wildcard cert**
```
sudo ./certbot-auto certonly --server https://acme-v02.api.letsencrypt.org/directory --agree-tos --manual --preferred-challenges dns -d 'atma.co.za,*.atma.co.za'
```

**Setup DHCP server in Virtualbox**
```
vboxmanage dhcpserver add --netname intnet --ip 10.10.0.1 --netmask 255.255.0.0 --lowerip 10.10.10.1 --upperip 10.10.10.255 --enable
```

**Send mail**
```bash
echo "test message" | mailx -s 'test subject' john@domain.com  
```

**Mount Windows Partition that is in hibernation mode**
```bash
sudo ntfsfix /dev/sda3 
sudo mount /dev/sda3 /mnt 
```

**Run a command every x seconds**
```bash
watch -n10 command args
```
