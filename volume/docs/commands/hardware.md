# Hardware information
**All hardware info**
```bash
sudo lshw | less
```

**Motherbaord**
```bash
sudo lshw -c system
```

**Network**
```bash
sudo lshw -C network
```

**CPU**
```bash
sudo lscpu 
```

**All PCI adapters**
```bash
sudo lspci 
```

**Graphics card**
```bash
sudo lspci | grep GPU
```

**Memory**
```bash
free -h 
```

