**Find port and protocal**
```bash
ufw status numbered |(grep '80/tcp'|awk -F"[][]" '{print $2}')
```

**Delete port**
```bash
ufw delete $(ufw status numbered |(grep '80/tcp'|awk -F"[][]" '{print $2}'))
```

**Delete firewall rules on cli and auto select yes**
```bash
yes | for i in {50..3}; do sudo ufw delete $i; done
```

**Disable IPv6 rules**
```bash
sudo vim /etc/default/ufw 
IPV6=no 
```
